Base-Packages
=========

This Ansible role install necessary base packages for building standard environments

Requirements
------------

- Ansible
- Python

Role Variables
--------------

Variable are dynimically changes pending the operating system family utilizing ansible facts

Example Playbook
----------------
```yml
---
- hosts: localhost
  connection: local
  gather_facts: yes
  vars:
    ansible_python_interpreter: /usr/bin/python3
  roles:
    - base-packages
```
License
-------

BSD
